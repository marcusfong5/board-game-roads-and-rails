## Board game - Roads and Rails
#### Author: Marcus Fong | Date last edited: 29th May 2019
This was a Group Java Development project I did. In this project, we used
AGILE software development and did a final presentation at the end.

The game we implemented was a boardgame called Roads and Rails. I mainly did
the scoring component, function development and code testing and UX 
(User Experience).

Click [here](https://drive.google.com/open?id=1_UJdhervGfmTfrjujAD0sOhJ3RSodKJA) for link to the project assets.